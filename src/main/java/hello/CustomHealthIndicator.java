package hello;

import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

@Component
public class CustomHealthIndicator implements HealthIndicator {

    @Override
    public Health health() {
        Boolean healthy = null;
        String details = "test";

        // perform your health check

        if (healthy == null) {
            return Health.unknown().withDetail("details", "Health check not implemented").build();
        } else if (healthy) {
            return Health.down().withDetail("details", details).build();
        } else {
            return Health.up().build();
        }
    }

}
