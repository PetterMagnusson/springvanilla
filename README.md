#SVT SpringBootVanilla

A very simple SpingBoot application used in SVTIs Tekniksprint 2015


URLs:

* http://localhost:8080
* http://localhost:8080/health

## Quick Start
Includes a gradle wrapper. Build and run with:
./gradlew build && java -jar build/libs/gs-spring-boot-0.1.0.jar

### Requirements
JAVA

